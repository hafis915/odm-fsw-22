const { Students } = require('../model') // Model.Students

class StudentsController {
  static async getStudents(req, res, next) {
    try {
      let data = await Students.find({})
      console.log(data)

      res.status(200).json(data)
    } catch {

    }
  }

  static async getStudent(req, res, next) {
    try {
      let _id = req.params.id
      let data = await Students.findOne({ _id })
      res.status(200).json(data)
    } catch (error) {

    }
  }

  static async createStudent(req, res, next) {
    try {
      const { Age, fullName } = req.body
      const payload = {
        fullName,
        Age
      }
      const data = await Students.create(payload)
      if (data) {
        console.log(data)
        res.status(200).json(data)
      }
    } catch (error) {
      console.log(error)
    }
  }

  static async updateStudent(req, res, next) {
    try {
      const { Age, fullName } = req.body
      const payload = {
        Age,
        fullName
      }
      const _id = req.params.id

      const data = await Students.findOneAndUpdate(
        { _id },
        payload,
        { new: true }
      )

      res.status(200).json(data)

    } catch (error) {
      console.log(error)
    }
  }
  static async deleteStudent(req, res, next) {
    try {
      const _id = req.params.id
      const data = await Students.deleteOne({ _id })
      console.log(data)
      res.status(200).json(data)
    } catch (error) {
      console.log(error)
    }
  }
}


module.exports = StudentsController
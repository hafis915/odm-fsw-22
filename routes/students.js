const express = require('express')
const router = express.Router()
const { StudentController } = require("../controller")
router.use(express.json())


router.get('/students', StudentController.getStudents)
router.get('/students/:id', StudentController.getStudent)
router.post('/students', StudentController.createStudent)
router.put('/students/:id', StudentController.updateStudent)
router.delete('/students/:id', StudentController.deleteStudent)



module.exports = router
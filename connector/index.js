const mongoose = require('mongoose')

exports.initialize = async () => {
  //  Belum pernah connect
  if (mongoose.connection.readyState === 0) {
    await mongoose.connect(
      'mongodb://localhost:27017/FSW-22',

    )
  }

  return mongoose
}
const express = require("express")
const mongoose = require('mongoose')
const app = express()
const routes = require("./routes/students")

mongoose.connect('mongodb://localhost:27017/FSW-22', { useNewUrlParser: true })
  .then(() => {
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use(routes)
    app.listen(5000, () => {
      console.log("Server has started!")
    })
  })




// const http = require('http')
// const databaseConnector = require('./connector')
// const app = express();

// databaseConnector.initialize().then(null, (err) => {
//   console.log(err.message)
//   process.exit(1)
// })

// const server = http.createServer(app)

// server.listen(3000)


// server.on('listening', () => {
//   const addr = server.address();
//   const bind = typeof addr === 'string'
//     ? `pipe ${addr}`
//     : `port ${addr.port}`;
//   console.info(`Listening on ${bind}`);
// })
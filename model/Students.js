const mongoose = require('mongoose')

const { Schema } = mongoose

const Students = new Schema({
  fullName: String,
  Age: Number
})

// const Students = mongoose.Schema({
//   fullName: String,
//   Age: Number
// })

module.exports = mongoose.model("Students", Students)